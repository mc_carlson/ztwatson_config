const express = require('express');
const morgan = require('morgan');
const bodyParser = require('body-parser');

const config = require('./modules/config');

var app = express();
app.use(morgan('combined'));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.get('/config', (req, res) => {
    res.send(config)
});

app.listen(process.env.PORT || 3001, (err) => {
    console.log('Listening on port: ', process.env.PORT || 3001);
});